#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266WebServerSecure.h>

const char *ssid = "motyl";
const char *password = "L6SQV7SR865UH";
const char *dname = "esp8266";

int moisture_value = 0;
int pump_status = 0;
time_t time_moisture_measure = time(NULL);
time_t time_pump_running = time(NULL);

#define analog_moisture_pin A0
#define vcc_moisture_pin D2
#define pump_pin D1


BearSSL::ESP8266WebServerSecure server(443);
ESP8266WebServer serverHTTP(80);

static const char serverCert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIICTDCCAbUCFG78snbyG2mrcKgu/vWsDHa/2Z2NMA0GCSqGSIb3DQEBCwUAMGUx
CzAJBgNVBAYTAkNaMQwwCgYDVQQIDANQUkcxDzANBgNVBAcMBlByYWd1ZTERMA8G
A1UECgwISDJvIFtST10xDDAKBgNVBAsMA0gybzEWMBQGA1UEAwwNZXNwODI2Ni5s
b2NhbDAeFw0yMTA0MjExMTU5NTdaFw0yMjA0MjExMTU5NTdaMGUxCzAJBgNVBAYT
AkNaMQwwCgYDVQQIDANQUkcxDzANBgNVBAcMBlByYWd1ZTERMA8GA1UECgwISDJv
IFtST10xDDAKBgNVBAsMA0gybzEWMBQGA1UEAwwNZXNwODI2Ni5sb2NhbDCBnzAN
BgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAtJcFRW4xFj+/aeoB++t2k6lqQoSV96Ts
/ADF0GCOj0GRMzD4/+yHyu4VO31zcOd2srpFZ0LXgWljimBHhseJDjK04pV57M0g
DX5m5+UjTJ3leOa3YgpAwSFQ4taDpbE+P2Zak22wp7gQDZE/ckkHeV83NEFGxpLy
Tpt2RMlvOgUCAwEAATANBgkqhkiG9w0BAQsFAAOBgQCVuWTJ7Gap+yQHbh3koSHj
Y9mzijPy5hFPS51ykSRlYQr+9uIyO7xV/4GmePjOVGRRt0pmB4IVDDiZbYRe/NSp
+JQg5RPysWp37wADwjTKD1sJF9Ad9wbx6vXe3gvENjgZUI2orxY7kDR9rVf3QY9R
Z6l3JoodbaJklwD0/7/+0A==
-----END CERTIFICATE-----
)EOF";

static const char serverKey[] PROGMEM =  R"EOF(
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC0lwVFbjEWP79p6gH763aTqWpChJX3pOz8AMXQYI6PQZEzMPj/
7IfK7hU7fXNw53ayukVnQteBaWOKYEeGx4kOMrTilXnszSANfmbn5SNMneV45rdi
CkDBIVDi1oOlsT4/ZlqTbbCnuBANkT9ySQd5Xzc0QUbGkvJOm3ZEyW86BQIDAQAB
AoGAQqdfBbJXIcwjnTcGxnGxEW09UBHZvE6+PYEXggKxe4SK0CcqMGOf9zGK2qtB
+ZElCXCg/yz3l8dLOx6wJ7nsBpc0I9T4Fhmrs1GLs1Gs0IdIisA9/fZWV62zrQl8
FVoS9syI9MIdAstED8hcgInuUb1Dt7D7ud7rVjN8cOGaoV0CQQDqjle1bga6M8Xg
graVSuIXegJWab+ka/ykPmZL8gIy4cVEtWjSMnvgkfp97MMe75AT0cdiQCUSawpW
/4THNtxfAkEAxRmhZTTrQHGmoZhoKUn2YfWgyGh+IBAIspaciHC8oJhOUl+WrEfL
jUCCxHP6/UZHxAJt6VnOhvtlbUbmIRuEGwJAXhdv09YpWLTAIkoNWGJLxEMrgOK3
AAxZssFdZAXXH/Y1nlRvkjKyQuoMZEuAEn7jey/iHeXBeVv7L98cspWtXQJBAJ7q
E17uU98Zwyf3IcRoheZljMUIuuhPBTQ4iuzXOL6jDkMMHwe4uz2gDc8nU+5xg9td
uhCy//mFKDn7OADplpECQCA+CUZ2Yf3WjLxptp7Y1dKsw0R29s4ZL/VyfzuvPxYk
jbf3GJF73tXprXw4j8FD8DnoWDnzS7c8I9W/13pUsXE=
-----END RSA PRIVATE KEY-----

)EOF";

bool connectToWifi() {
  byte timeout = 50;

  Serial.println("\n\n");

  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  for (int i = 0; i < timeout; i++) {
    if (WiFi.status() == WL_CONNECTED) {
      Serial.println("\nConnected to WiFi");
      Serial.print("Server can be accessed at https://");
      Serial.print(WiFi.localIP());
      if (MDNS.begin(dname)) {
        // https://superuser.com/questions/491747/how-can-i-resolve-local-addresses-in-windows
        Serial.print(" or at https://");
        Serial.print(dname);
        Serial.println(".local");
      }
      return true;
    }
    delay(5000);
    Serial.print(".");
  }

  Serial.println("\nFailed to connect to WiFi");
  Serial.println("Check network status and access data");
  Serial.println("Push RST to try again");
  return false;
}

void runPump ()
{
  digitalWrite(pump_pin, HIGH);
  time_pump_running = time(NULL);
  Serial.print("pump is running");

  Serial.println();
  pump_status = 1;
}

void measureMoisture ()
{
  digitalWrite(vcc_moisture_pin, HIGH);
  delay(100);
  moisture_value = analogRead(analog_moisture_pin);

  digitalWrite(vcc_moisture_pin, LOW);
  time_moisture_measure = time(NULL);
  Serial.print("Soil moisture val: ");
  Serial.print(moisture_value);
  
  Serial.println();

  if (moisture_value > 550)
  {
    runPump();  
  }
  
}

void showWebpage() {

  
  if (server.hasArg("measure_moisture")) {
    String status = server.arg("measure_moisture");
    if (status == "measure") {
      measureMoisture();
    }
  }

  String content = "<!DOCTYPE html><html>";
  content += "<head><title>ESP8266 Test Server</title>";
  content += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
  content += "<link rel=\"shortcut icon\" href=\"data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAA/4QAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARAiIgEQAiAQASAAEAEgAhABAgAQASACARAAIBEQIiAQASACAQACABABIAIAEAAgARACIAABAAIAAAAAAAAAAAAAAAAAAAAAEREAERAAEAAQAAAAAQAQABAAAAABABAAERAAARAAERAQAAAQAAAQARAAABAAABABEREAAREAERCQmQAAZ2YAAGtmAACdEQAAZrsAAGbdAACZ7gAA//8AAP//AAAMdwAAf7cAAH+3AAAecQAAffYAAH32AAAOMQAA\" />";
  content += "<style>body {font-family: sans-serif;} </style>";
  content += "</head><body>";
  content += "<h1>ESP8266 HTTPS Server</h1><form action=\"/\" method=\"get\">";

  
  content += "<input type=\"submit\" value=\"measure\" name=\"measure_moisture\"><br><br>";
  content += "<input type=\"submit\" value=\"pump\" name=\"run_pump\">";
  content += "<h3> Soil Moisture value: " + String(moisture_value) + "</h3>";
  content += "</form>";


  if (server.hasArg("run_pump")) {
    String status = server.arg("run_pump");
    if (status == "pump") {
      runPump();
    }
  }

  if (pump_status) content += " <i>Pump is turned on</i>";
  else content += " <i>Pump is turned off</i>";

  content += "<p>";
  content += "Last time of soil moisture measurement: ";
  content += ctime(&time_moisture_measure);
  content += "</p>";
  content += "<p>";
  content += "Last time water pumping: ";
  content += ctime(&time_pump_running);
  content += "</p>";
 
  server.send(200, "text/html", content);
}

void secureRedirect() {
  serverHTTP.sendHeader("Location", String("https://esp8266.local"), true);
  serverHTTP.send(301, "text/plain", "");
}

void setup() {
  pinMode(analog_moisture_pin, INPUT);
  pinMode(vcc_moisture_pin, OUTPUT);
  pinMode (pump_pin, OUTPUT);
  Serial.begin(115200);

  if (!connectToWifi()) {
    delay(60000);
    ESP.restart();
  }

  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");

  serverHTTP.on("/", secureRedirect);
  serverHTTP.begin();

  server.getServer().setRSACert(new BearSSL::X509List(serverCert), new BearSSL::PrivateKey(serverKey));
  server.on("/", showWebpage);
  server.begin();
  
  Serial.println("Server is ready");
}

void loop() {
  serverHTTP.handleClient();
  server.handleClient();
  MDNS.update();

  if (time(NULL) - time_moisture_measure > 18000) {
     measureMoisture();  
  }

  if (digitalRead(pump_pin))
  {
    if (time(NULL) - time_pump_running > 20)
    {
      digitalWrite(pump_pin, LOW);
      pump_status = 0;
    }
  }
    
  
}
