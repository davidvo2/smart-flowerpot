# Chytrý květináč

Cílem této práce je vztvořit zařízení, které bude sledovat vlhkost půdy a na tomto základě pouštět čerpadlo, když je potřeba půdu zvlhčit. 

Také je zařízení připojené k  Wi-fi a bezpečeně komunikuje s klienty, kteří pře webovo rozhranní mohou pouštět čerpadlo, nebo sledovat vlhkost půdy. 

